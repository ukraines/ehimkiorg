﻿<?

/* "
UPDATE `titles`
SET `russian` = `english`

WHERE russian = 'xxx'
AND `lecture` = '4'
AND `english` != "";

WHERE `russian` LIKE '%xxx%'
AND `lecture` = '4'
AND `english` != ""
*/

// ".\r\n\r\n"

$lang['en'] = 'en-US';
$lang['ru'] = 'ru-RU';



if (empty($_GET['lang'])) $_GET['lang'] = "en";

$lecture = $_GET['lecture'];
define("LECTURE",$lecture);

// Данные для доступа к БД
// DB login details
define("DB_HOST","localhost");
define("DB_USER", "trof");
define("DB_PASS", "6iB3PJ6hkCJS5winYtQiQ");
define("DB_NAME", "trof");

$link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

// $fileName = "How to Build a Product I, Michael Seibel, Steve Huffman, Emmett Shear - CS-183F." . $lang[$_GET['lang']] . ".srt";
// echo $fileName; // $fileName = "How to Get Ideas and How to Measure - Stewart Butterfield & Adam D'Angelo." . $lang[$_GET['lang']] . ".srt";

$fileName = "How to Build a Product III - Jason Lemkin, Solomon Hykes, Tracy Young and Harry Zhang - CS183F." . $lang[$_GET['lang']] . ".srt";
$fileName = "Startup Mechanics - Kirsty Nathoo - CS183F." . $lang[$_GET['lang']] . ".srt";
$fileName = "How to Build a Product IV - Jan Koum - CS183F." . $lang[$_GET['lang']] . ".srt";

// echo $fileName;

// Соединяемся и выбираем нужную базу данных

$titles = trim(file_get_contents($fileName));

echo "
Params:

<br><b>&page=plainText</b> show as a non separated text without timecodes from the database<br>
<b>&page=insertOriginal</b> generates SQL code for the subtitles read from the LANG file to be inserted for a particular lecture <br>

<b>&lecture=NUM</b> number of a lecture to work with<br>
<b>&lang=en|ru</b> language Subtitles file to work with<br>

<br>
Lang <b>" . $lang[$_GET['lang']] . "</b><br>\n
Lecture <b>" . LECTURE . "</b><br>\n
Sentences <b>" . substr_count($titles, '.') . "</b><br>\n
Questions <b>" . substr_count($titles, '?') . "</b><br>\n
\n";

// English titles
$titles = explode("

",$titles);


// Russian titles
/* $titles = explode("



",$titles); */

// print_r($titles);

function insertOriginalTitles($titles, $lecture, $insertUpdate="") {

	foreach ($titles as $key=>$val) {

// russian

	/* $props = explode("



	", $val); */

// English

$props = explode("
", $val);



// print_r($props);

switch($insertUpdate) {

	default:

		$values .= "($lecture, '$props[0]', '$props[1]', '$props[2]'),\n\n";

	break;

	case "showContentLinesOnly":

		$values .= "$props[2]\n";

	break;

	case "update";

		$values .= "update `titles`	set `russian` = '" . trim("$props[2]\n$props[3]") . "'
		where `number` = '$props[0]'
		and lecture = $lecture;\n\n";

	break;

	case "updateRussian";

$i++;

	$values .= "update `titles`	set `russian` = '" . trim("$props[2]") . "'
	where `number` = '$i'
	and lecture = $lecture;\n\n";

	break;


}

}

return "<br>\n\nINSERT INTO
`titles` (lecture, number, timestamp, english) values
$values ";

}


function insertRussianTitles($titles) {

	foreach ($titles as $key=>$val) {

	$i++;

	$values .= "update titles set russian = '$val' where number = $i;\n\n";

	}

	return "INSERT INTO
	`titles` (number, english) values
	$values ";

}

function generateTitles($link, $action="") {

// AND INSTR('.', `russian`) = 0 // in a case to show lines without dot

$sql = "SELECT * FROM `titles` WHERE lecture ='" . LECTURE . "'";
// $sql = "SELECT * FROM `titles` WHERE `english` LIKE '%.%' and lecture = 4";
// $sql = "SELECT * FROM `titles` WHERE `english` LIKE '%>>%' and lecture = 4";
// $sql = "SELECT * FROM `titles` WHERE `lecture` = 4 AND INSTR('.', `english`) = 0";

	$sql_res = mysqli_query($link, $sql);
	$num_rows = mysqli_num_rows($sql_res);

echo "Subtitles <b>$num_rows</b><br>\n\n";

	for ($i=0; $i<$num_rows; $i++) {
		$row = mysqli_fetch_array($sql_res);


	switch ($action) {

	default:

		$values .= "$row[number]\n$row[timestamp]\n" . trim($row['russian']) . "\n\n";
		break;

	case "plainText":

		$values .= trim($row['english']) . "\n";
		break;

	case "compareVersions":

		if (substr_count($row['russian'], '>>') === 0) {

			$values .= "<tr>
			<td>$row[number]</td>
			<td>" . $newstring = substr(trim($row['english']), -20) . "</td>
			<td>" . $newstring = mb_substr(trim($row['russian']), -20) . "</td>
			</tr>
			";

		}

		break;

	}

}

// $values = "<br>total chars: ". strlen($values) . "<br><br>\n\n" . $values;

return $values;

}

function displayTextAs($content, $type="") {

	$srch = array("\n", "\r", "  ");
	$content = str_replace($srch," ", $content);

	$srch = array(". ", "? ", ">>", "\n\n\n\n");
	$rplc = array(".\n\n", "?\n\n", "\n\n>>", "\n\n");

	$content = str_replace($srch, $rplc, $content);

	switch ($type) {

		default:
		break;

		case "split":
		$preContent = str_split($content, 2500);

		$content = "";

		foreach ($preContent as $key=>$val) { $i++; $content.="\n\n\n\nPart $i:\n\n$val"; }

		break;

	}

	return $content;

}



// echo "total rows found $sql_res";

switch($_GET['page']) {

	default:

		echo generateTitles($link);
		break;

	case "plainText";

		$content = generateTitles($link, "plainText");

		$lines_arr = preg_split('/\n/',$content);
		$num_newlines = count($lines_arr);
		echo "Source lines <b>$num_newlines</b><br><br>\n\n";
		echo $content;
		// echo displayTextAs($content);
		break;

	case "compareVersions":

		echo "<table border='1'>" . generateTitles($link, "compareVersions") . "</table>";
		break;

	case "countLines":

		echo countLines($fileName);
		break;

	case "updateRussianEditedTitles":

		echo insertOriginalTitles($titles, LECTURE, 'update');
		break;

	case "insertOriginal":

	 echo insertOriginalTitles($titles,LECTURE);

	break;

}



//
//



?>
