<?

$dataCommercial = "

Изучение английского %%% Индивидуальные и групповые занятия, 14+ %%% main-picture-eng-website.jpg
Программирование на заказ %%% интернет-магазины, калькуляторы, crm, автоматизация
Письменные и устные переводы %%% любые языки, любые города и страны, локализация %%% interpreterworldwide.jpg
Коворкинг %%% персональные рабочие места для удаленных сотрудников и фриланса %%% coworking-14.jpg
Бесплатные Экспертные часы %%% консультации по локализации, пиару, программированию, seo
Бесплатные Мастер-классы %%% по созданию сайтов без программирования %%% website-create-no-knowledge.jpg
Бесплатные Мастер-классы %%% по поисковому продвижению товаров и услуг
Бесплатные Мастер-классы %%% по продвижению в социальных сетях 
Английский язык %%% бесплатные групповые занятия по изучению английского для взрослых %%% enlish-meetup-himki.jpg
Митапы для предпринимателей %%% встречи предпринимателей и стартаперов, обмен опытом %%% entrepreneur-meetup.jpg


";

$dataEventsSchedule = "

Вт
Испанский с нуля
19:00

%%%

Ср
Лекции по предпринимательству Y Combinator
19:00

%%%


Чт
Разговорный английский
19:00

%%%

Пт
Немецкий для начинающих
19:00

%%%

Следите за анонсами
Мастер-классы по предпринимательству
-

%%%

Следите за анонсами
Основы поискового продвижения
-

%%%

Следите за анонсами
Мастер-класс по созданию сайтов без навыков программирования
-

%%%

Следите за анонсами
Основы создания веб-страниц (HTML/CSS)
-



";

$dataReviews = "

Дмитрий
Мастер-класс \"Основы HTML/CSS\"
https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-9/320487_1959649797859_4916164_n.jpg?oh=0a913299788385d096d25622f0a4821a&oe=595C069B
https://www.facebook.com/profile.php?id=1441211577&fref=nf
https://www.facebook.com/events/241478819648535/
Сходил на пробное занятие, размял немного мозги. Во-первых очень понравилось место. Очень уютно и комфортно. Во-вторых, очень благоприятное впечатление произвел Юрий. Сразу видно, что он человек образованный, умный, увлеченный своим делом. По поводу занятия: считаю себя продвинутым пользователем, но без опыта создания страничек. За время занятия (кажется пролетело часа два) с помощью Юрия вполне освоил некие начала и логику работы с HTML. В принципе все оказалось вполне не сложно и понятно. Материала в интернете достаточно, так что думаю, что буду и дальше разбираться. Юрий, громадное спасибо за данный старт новому увлечению! До новых встреч!

";

// Какие сейчас проходят события. По вторникам в 19:00 встреча по <b>изучению испанского с нуля</b>. <br>По средам в 19:00 мастер-классы по <b>программированию, основам поискового продвижения, генерации идей, созданию сайтов без навыков программирования</b>. <br>По четвергам в 19:00 встреча по <b>изучению английского</b>. <br>По пятницам в 19:00 встреча по <b>изучению немецкого</b> %%%

$dataAbout = "

Как узнать точное расписание мероприятий?
Обязательно подписываемся на страницу <a href='//www.facebook.com/startupHimki'>СтартапХИМКИ</a> и <a href='//www.facebook.com/profile.php?id=100011222233927&ref=bookmarks'>Юрий Ярцев</a>, а также канал <a href='//telegram.me/startupHimki'>СтартапХимки телеграм</a>  и следим за анонсами событий

%%%

Как записаться на мероприятие?
Бесплатные встречи обычно проходят по вторникам и четвергам. Пишем в личку (<a href='//vk.com/banduran'>вк</a>, <a href='//www.facebook.com/profile.php?id=100011222233927'>фб</a>) свое фио, телефон и желаемое мероприятие

%%%

Где вы находитесь?
В БЦ Аэросити напротив ТЦ Hoff и БЦ Кантри Парк<br><strong>Адрес</strong>: Москва, Куркинское шоссе, строение 2, здание БЦ Аэросити, 14 этаж, помещение Коворкинг 14<p><p><strong>Пользователям навигатора Google Maps</strong>. Если указать точный адрес, заедете в совершенно другую сторону. <br><strong>Решение проблемы:</strong> вбивайте в строку поиска 'Коворкинг 14'. <br><br><img src='http://upro.bz/uploads/image/file/11775/watermarked_1.jpg'>

%%%

Проезд на общественном из Куркино
Остановка '8-мкр Куркина'. Автобус 268 и 268К, маршрутка 980. Едем до остановки мкрн 2Б Химки. <br> Оттуда минут 7 пешком по Бабакина до Кантри Парка, далее слегка направо будет виден новый высокий Бизнес Центр<br><br><img src='kurkino-vorot-him-2b.jpg'>

%%%

Проезд на общественном из Химки, мкрн Левобережный
Остановка 'Улица Пожарского'. Автобусы и маршрутки 32 и 32С и 5. Едем до остановки Бутаково. <br> Оттуда минуты 3 пешком по Куркинскому шоссе. Впереди справа будет виден высокий цветастый Бизнес Центр<br><br><img src='images/leviy-bereg.jpg'><br><a href='//yandex.ru/maps/10758/himki/?ll=37.439048%2C55.883421&z=15&mode=routes&masstransit%5BstopId%5D=stop__9691897&rtext=55.892531%2C37.483099~55.883796%2C37.434501&rtt=mt&rtm=atm'>Маршрут на яндексе</a>

%%%

Проезд на общественном из Москвы
<strong>От м. Войковская</strong>: Первый вагон из центра и в переходе направо, далее на 309м. Ехать до ост. Бутаково<br><strong>От м. Планерная и Речной вокзал</strong>, смотрите схему: <br><br><img src='//pp.vk.me/c626523/v626523745/3e4c2/cWuPbGiiFlE.jpg'>

%%%

Проезд от станции Химки. 
Маршрутка 469 или 51. <br><br><img src='images/station-himki.jpg'><br><a href='https://www.google.ru/maps/dir/55.894741,37.449987/55.8836682,37.4348802/@55.8882737,37.4327406,15z/data=!4m2!4m1!3e3?hl=ru'>Карта</a>

%%%

Проезд на своем авто
Внимание! Пользователям навигатора Google Maps - вбивайте в строку поиска 'Коворкинг 14'. Если указать адрес выше, заедете в совершенно другую сторону<br><br><iframe src='//www.youtube.com/embed/y1Of7xbzW2w' frameborder='0' allowfullscreen></iframe>

%%%

Есть ли парковка?
Да, в здании находится подземный паркинг за 200 р целый день.

%%%

Что брать с собой на событие?
На <b>встречи по изучению иностранных языков</b> берите блокнот, ручку, хорошее настроение и, возможно, какие-то сладости к чаю и кофе. <br> На мастер-классы по поисковому продвижению, основам программирования HTML и созданию сайтов- дополнительно берите ноутбук или планшет.

%%%

Есть ли возможности сотрудничества или стажировок?
<br>Да, ищем носителей английского и других языков, в том числе немецкого, итальянского и испанского для сотрудничества<br>Ищем стажеров/интернов для фото/видеосъемки событий. Мы покажем и расскажем, что и как снимать. Оборудование есть<br>Ищем программистов, готовых раз в месяц посвящать час своего времени бесплатному обучению всех желающих своему ремеслу

";


function generateContent($data) {

	$data = explode("%%%", trim($data));

	foreach ($data as $key=>$val) {

		$props = explode("\n", trim($val));

		$i++;

		echo "
		
								<div class='inner spotlighty'>
									<header class='major'>
										<h3><a name='$i'>$props[0]</a></h3>
									</header>
									<p>$props[1]</p>
									<!-- <ul class='actions'>
										<li><a href='landing.html' class='button next'>Get Started</a></li>
									</ul> -->
								</div>

	";

	}
}

function generateTiles($data) {

	$data = explode("\n", trim($data));

	foreach ($data as $key=>$val) {

		$props = explode("%%%", trim($val));

		$props = array_map("trim", $props);

		echo "
		
	<article style='background-image: url(&quot;images/$props[2]&quot;);'>
		<span class='image' style='display: none;'>
			<img src='images/$props[2]' alt=''>
		</span>
		<header class='major'>
			<h3><a href='javascript:void()' class='link'>$props[0]</a></h3>
			<p>$props[1]</p>
		</header>
	<a href='javascript:void()' class='link primary'></a></article>

	";

	}
}


function generateReviews($data) {

	$data = explode("%%%", trim($data));

	foreach ($data as $key=>$val) {

		$props = explode("\n", trim($val));

		echo "
		

<!-- Дмитрий
//www.facebook.com/profile.php?id=1441211577&fref=nf
Основы HTML/CSS
//scontent-arn2-1.xx.fbcdn.net/v/t1.0-9/320487_1959649797859_4916164_n.jpg?oh=0a913299788385d096d25622f0a4821a&oe=595C069B
//www.facebook.com/events/241478819648535/ -->

								<section>
									<div class='contact-method'>
										<span class=''></span>

								<div class='inner spotlighty'>
										<h3>$props[1]</h3>
										<blockquote>$props[5]</blockquote>
										<div><a href='$props[1]' target='_blank'>$props[0]</a></div>
								</div>

									</div>
								</section>

	";

	}
}

function generateSchedule($data) {

	$data = explode("%%%", trim($data));

	foreach ($data as $key=>$val) {

		$props = explode("\n", trim($val));

		echo "
		
<tr>
<td>$props[0]<td>
<td>$props[1]<td>
<td>$props[2]<td>
</tr>


	";

	}
}


?><!DOCTYPE HTML>
<!--
	Forty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Бесплатные события в Химки от СтартапХимки и Юрий Ярцев</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<style>.spotlighty img, iframe {
		
			border-radius: 2px;
			display: block;
			width: 100%;

		}
		
		iframe {height: auto; min-height: 600px;}
		</style>
		<script>
// to top right away
if ( window.location.hash ) scroll(0,0);
// void some browsers issue
setTimeout( function() { scroll(0,0); }, 1);

$(function() {

    // your current click function
    $('.scroll').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top + 'px'
        }, 1000, 'swing');
    });

    // *only* if we have anchor on the url
    if(window.location.hash) {

        // smooth scroll to the anchor id
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top + 'px'
        }, 1000, 'swing');
    }

});;
		</script>
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">


				<!-- Banner -->
					<section id="banner" class="major">
						<div class="inner">
							<header class="major">
								<h1>СтартапХИМКИ</h1>
							</header>
							<div class="content">
								<p>Экосистема для увлеченных и объединенных страстью к созиданию, постоянному самообразованию, самосовершенствованию и предпринимательству. Здесь всё для воплощения мечты, начиная с бесплатных мероприятий по изучению иностранных и языков программирования до формирования команды вашего будущего "единорога". Говорим "нет" невозможному и недостижимому</p>
								<ul class="actions">
									<!-- <li><a href="#one" class="button next scrolly">Get Started</a></li> -->
								</ul>
							</div>
						</div>
					</section>

<div id="main">

						<!-- One -->
							<section id="one" class="tiles">

								<? generateTiles($dataCommercial); ?>
							
							</section>

						
						<!-- Two -->
							<section id="two">

								<div class='inner spotlighty'>
									<header class='major'>
										<h2>Расписание мероприятий</h2>
									</header>

								<table>
								<? generateSchedule($dataEventsSchedule) ?>
								</table>

								</div>

								<? generateContent($dataAbout); ?>
							
							</section>

							<section id='three'>
								
								<div class='inner spotlighty'>
								<h2>Отзывы</h2>
								<? generateReviews($dataReviews); ?>
								</div>

							</section>

							<footer id="footer">
							<div class="inner">
								<ul class="icons">
									<!-- <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li> -->
									<li><a href="http://www.facebook.com/startupHimki" class="icon alt fa-facebook"><span class="label">ФБ</span></a></li>
									<li><a href="http://vk.com/startupHimki" class="icon alt fa-vkontakte"><span class="label">ВК</span></a></li>
									<!-- <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
									<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
									<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li> -->
								</ul>

							<p>Связь с Юрием через <a href='//vk.com/banduran'>Вконтакт</a> или <a href='//www.facebook.com/profile.php?id=100011222233927'>ФБ</a>

								<ul class="copyright">
									<li>© Untitled</li><li>Design: <a href="//html5up.net" target='_blank'>HTML5 UP</a></li><li>Demo Images: <a href="//unsplash.com" target='_blank'>Unsplash</a></li>
								</ul>
							</div>
							</footer>


<!-- формат: смотрим, записываем незнакомые слова, обсуждаем, пытаясь понять их.
Сейчас никто не корректирует. Когда наберется группа и ядро, будем нанимать носителя-->

<!-- CheckList

Новеньким - заполнить анкету
Тем, кто пришел - попросить отзыв
Просьба ко всем лайкнуть //www.facebook.com/startupHimki
Просьба поставить оценку конкретному прошедшему мероприятию
-->