<?

$lang['en'] = "

$weblieve = We believe the barrier to entry for people to start a startup is still too high. We want to make it easier for people to start a company, regardless of who or where you are, so we're starting by sharing what we've learned, through Startup School.",
Startup School is a free 10-week massively open online course (MOOC). The course will begin on April 5th, 2017. Lectures will be posted here weekly.
Got a startup? Sign up for Startup School.
For founders actively pursuing your own startup, we welcome you to participate in the course. You will have additional access to active founder-advisors who can help give you feedback on your company.
If you're interested in participating as a startup founder, or receiving course updates and announcements, sign up here and we'll let you know when registration opens:
No startup? Get inspired.
Looking for the  Startup School Conference
Startup School by Y Combinator.
All rights reserved.
About Y Combinator Startup School
Our goal
Throughout the 10 weeks, we aim to accomplish the following:
Encourage and inspire people to consider starting a company as a way to positively impact the world
Teach people about how to start a startup, and equip them with the resources and tools to help prepare them now and in the future
Build a community of entrepreneurs who can encourage and teach each other
Course Format
Tuesdays - Lectures
Lectures will be taught by a diverse group of entrepreneurs and industry leaders. We'll have some familiar faces from Sam's Startup Class (http://startupclass.samaltman.com/) and new speakers that we will announce closer to the start of the course.
Week 1: How and Why to Start A Startup, and Mechanics
Week 2: How to Get Ideas
Week 3: How to Build a Great Product I
Week 4: How to Build a Great Product II
Week 5: How to Get Users
Week 6: How to Win
Week 7: How to Execute
Week 8: How to Build a Good Culture
Week 9: How to Build and Manage Teams
Week 10: How to Raise Money, and How to Succeed Long-Term
Thursdays - Class Office Hours
Class Office Hours will also be held each week to give live advice and feedback to the class.
Participation
Participation in the course will follow two tracks:
Spectator
If you would like to learn but are not actively working on your startup.
You can follow along and have full access to all 10 lectures and class office hours, released once a week, here: https://startupschool.org/. No sign-up required.
Startup Founder
If you or your team are actively pursuing your startup and are interested in participating, sign up here: https://startupschool.org/.
In addition to having full access to all the lectures and class office hours, you will also:
Be assigned to a group of students and a group Advisor. Advisors are members of the Y Combinator alumni community who are currently running their own startup.
Have advisory sessions once every week via online video, and as needed via email
Have access to a Slack room with the other students in your group
Be responsible for updating your advisor on progress once a week
Course evaluation
You must submit at least 9 of your 10 weekly updates and attend 9 out of 10 group office hour sessions.
Upon successful completion, you will receive a certificate of completion and the opportunity to share what you're working on with the public.



";

$lang['ru'] = "

Мы полагаем, что барьер для входа для людей начать стартап все еще слишком высок. Мы хотим облегчить людям создание компании независимо от того, кто или где вы находитесь, поэтому мы начинаем с того, что рассказываем о том, что узнали, через Startup School.
Startup School - это бесплатный 10-недельный широко открытый онлайн-курс (MOOC). Курс начнется 5 апреля 2017 года. Лекции будут проводиться каждую неделю.
Получил стартап? Зарегистрируйтесь в Startup School.
Для основателей, активно занимающихся собственным запуском, мы приглашаем вас принять участие в этом курсе. У вас будет дополнительный доступ к активным основателям-советникам, которые помогут вам получить отзывы о вашей компании.
Если вы заинтересованы в участии в качестве учредителя стартапа или при получении обновлений курса и объявлений, зарегистрируйтесь здесь, и мы сообщим вам при регистрации:
Нет запуска? Вдохновиться.
Поиск школьной конференции Startup
Начальная школа Y Combinator.
Все права защищены.
О начальной школе Y Combinator
Наша цель
На протяжении 10 недель мы стремимся к достижению следующих целей:
Поощрять и вдохновлять людей на то, чтобы рассмотреть возможность создания компании как способ позитивно повлиять на мир
Научите людей, как начать стартап, и снабжайте их ресурсами и инструментами, чтобы помочь в их подготовке сейчас и в будущем.
Создайте сообщество предпринимателей, которые могут поощрять и учить друг друга
Формат курса
Вторник - Лекции
Лекции будут преподавать различные группы предпринимателей и лидеров отрасли. У нас будут знакомые лица из Samup Startup Class (http://startupclass.samaltman.com/) и новые докладчики, которые мы объявим ближе к началу курса.
Неделя 1: как и зачем начинать стартап и механика
Неделя 2: Как получить идеи
Неделя 3: Как создать отличный продукт I
Неделя 4: Как построить отличный продукт II
Неделя 5: Как получить пользователей
Неделя 6: Как победить
Неделя 7: Как выполнить
Неделя 8: Как построить хорошую культуру
Неделя 9: как создавать и управлять командами
Неделя 10: Как поднять деньги и как преуспеть в долгосрочной перспективе
Четверг - Часы работы класса
Часы работы класса также будут проводиться каждую неделю, чтобы дать живой совет и обратную связь с классом.
Участие
Участие в курсе будет проходить по двум направлениям:
Зритель
Если вы хотите учиться, но не активно работаете над своим запуском.
Вы можете следить и иметь полный доступ ко всем 10 лекциям и учебным рабочим дням, которые выходят раз в неделю, здесь: https://startupschool.org/. Никакой регистрации не требуется.
Создатель стартапа
Если вы или ваша команда активно участвуете в запуске и заинтересованы в участии, зарегистрируйтесь здесь: https://startupschool.org/.
Помимо полного доступа ко всем лекциям и занятиям в классе, вы также:
Назначаться группе студентов и советнику группы. Советники являются членами сообщества выпускников Y Combinator, которые в настоящее время запускают собственный запуск.
Проводите консультацию один раз в неделю через онлайн-видео и, если необходимо, по электронной почте
Получите доступ к комнате Slack с другими учащимися в вашей группе
Нести ответственность за обновление вашего советника о прогрессе раз в неделю
Оценка курса
Вы должны представить не менее 9 из 10 своих еженедельных обновлений и посещать 9 из 10 групповых часовых занятий.
После успешного завершения вы получите свидетельство о завершении и возможность поделиться тем, над чем вы работаете с общественностью.

";